{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Data.Maybe
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))

import GameInitModel
import CarPositionsModel

type ClientMessage = String

joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"
turboMessage = "{\"msgType\":\"turbo\",\"data\":\"All your laps are belong to me\"}"
laneSwitchMessage direction = "{\"msgType\":\"switchLane\",\"data\":\"" ++ direction ++ "\"}"
createRaceMessage botname botkey track password nCars = concat ["{\"msgType\":\"createRace\",\"data\":{",
                                                                  "\"botId\":{",
                                                                      "\"name\":\"" ++ botname ++ "\",",
                                                                      "\"key\":\"" ++ botkey ++ "\"},",
                                                                    "\"trackName\":\"" ++ track ++ "\",",
                                                                    "\"password\":\"" ++ password ++ "\",",
                                                                    "\"carCount\":\"" ++ nCars ++ "\"}}"]
joinRaceMessage botname botkey track password nCars = concat ["{\"msgType\":\"joinRace\",\"data\":{",
                                                                "\"botId\":{",
                                                                    "\"name\":\"" ++ botname ++ "\",",
                                                                    "\"key\":\"" ++ botkey ++ "\"},",
                                                                  "\"trackName\":\"" ++ track ++ "\",",
                                                                  "\"password\":\"" ++ password ++ "\",",
                                                                  "\"carCount\":\"" ++ nCars ++ "\"}}"]


connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey, action, track, password, carCount] -> do
      run server port botname botkey action track password carCount
    [server, port, botname, botkey] -> do
      run server port botname botkey "default" "" "" ""
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey> [[join|create] <track> <password> <number of racers]"
      exitFailure

run server port botname botkey action track password carCount = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  let a = joinRaceMessage botname botkey track password carCount
  case action of
    "create" -> hPutStrLn h (createRaceMessage botname botkey track password carCount)
    "default" -> hPutStrLn h (joinMessage botname botkey)
    "join" -> hPutStrLn h (joinRaceMessage botname botkey track password carCount)
    _ -> putStrLn $ "oops"
  handleMessages blankInitGame (CarId "" "") h

handleMessages game car h = do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success serverMessage -> handleServerMessage game car h serverMessage
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

data ServerMessage = Join | YourCar CarId | GameInit GameInitData | CarPositions [CarPosition] | Unknown String

handleServerMessage :: GameInitData -> CarId -> Handle -> ServerMessage -> IO ()
handleServerMessage game car h serverMessage = do
  (responses, newCar, newGame) <- respond game car serverMessage
  forM_ responses $ hPutStrLn h
  handleMessages newGame newCar h

-- Hard coded lameness for filtering our car
ourCar :: CarId -> String -> Bool
ourCar car name
    | name == carIdName car = True
    | otherwise             = False

-- Get our car position data from the list
getOurCar :: CarId -> [CarPosition] -> [CarPosition]
getOurCar car cars = filter ((ourCar car) .carIdName . CarPositionsModel.carId) cars

-- Short cut to get our car's piece position data
ourPiecePosition :: CarId -> [CarPosition] -> PiecePosition
ourPiecePosition car carPositions = piecePosition $ head $ getOurCar car carPositions

-- length of a turn piece
arcLength :: Piece -> Float
arcLength p = (pi * fromIntegral (fromJust (radius p)) * (fromJust (pieceAngle p))) / 180.0

-- quick check for turn piece
isTurnPiece :: Piece -> Bool
isTurnPiece p
    | (isNothing . radius) p = False
    | otherwise = True

-- is next piece a turn?
isNextATurn :: GameInitData -> CarId -> [CarPosition] -> Bool
isNextATurn g car p = isTurnPiece ((piecesOfGame g) !! (nextPiece (piecesOfGame g) (pieceIndex (ourPiecePosition car p))))

-- Are we in a turn piece?
inTurnPiece :: GameInitData -> CarId -> [CarPosition] -> Bool
inTurnPiece g car p = isTurnPiece ((piecesOfGame g) !! (pieceIndex (ourPiecePosition car p)))

-- Get the length of one lap
lapLength :: [Piece] -> Float
lapLength []     = 0.0
lapLenpth (x:xs)
    | isTurnPiece x = arcLength x + lapLength xs
    | otherwise = fromJust (pieceLength x) + lapLength xs

-- quick way to get the current position on the lap
currentPosition :: CarPosition -> Int
currentPosition p = pieceIndex $ piecePosition p

-- loop around for laps
nextPiece :: [Piece] -> Int -> Int
nextPiece p idx
    | idx >= (length p) - 1 = 0
    | otherwise = idx + 1

-- Identify the next corner piece index
-- This is used for finding the distance to it
getNextCorner :: GameInitData -> Int -> Int
getNextCorner g p
    | isTurnPiece ((piecesOfGame g) !! p) = p
    | otherwise = getNextCorner g (nextPiece (piecesOfGame g) p)

-- Distance remaining in current piece for both turns and straight pieces
distanceLeftInPiece :: GameInitData -> PiecePosition -> Float
distanceLeftInPiece g p =
    let pIndex   = pieceIndex p             -- Int
        ipd      = inPieceDistance p        -- Float
    in  (getPieceLength g pIndex) - ipd

-- Piece length for both turn and straight pieces
getPieceLength :: GameInitData -> Int -> Float
getPieceLength g p =
    let piece = (piecesOfGame g) !! p
    in fromMaybe (arcLength piece) (pieceLength piece)

-- Distance between two pieces
-- ps = piece start
-- pe = piece end
distanceToFrom :: GameInitData -> Int -> Int -> Float
distanceToFrom g ps pe =
    if ps == pe
    then 0
    else (getPieceLength g ps) + (distanceToFrom g (nextPiece (piecesOfGame g) ps) pe)

-- Distance from current position to entry point of the next corner
-- currentR   - current remaining distance
-- curIdx     - current piece index
-- nextCrnIdx - piece idx of the next corner
distanceToNextCorner :: GameInitData -> PiecePosition -> Float
distanceToNextCorner g p =
    let currentR = distanceLeftInPiece g p
        curIdx  = pieceIndex p
        nextCrnIdx = getNextCorner g (nextPiece (piecesOfGame g) curIdx)
    in  currentR + distanceToFrom g curIdx nextCrnIdx

-- Determine if the current position is half way through a piece
-- used for throttle adjustment in corners
overHalf :: GameInitData -> PiecePosition -> Bool
overHalf g p
    | distanceLeftInPiece g p / getPieceLength g (pieceIndex p) >= 0.9 = True
    | otherwise = False

-- Simple logic for handling how to drive
whatDo :: GameInitData -> CarId -> [CarPosition] -> (String, [ClientMessage])
whatDo g car cps
    | ((not (isNextATurn g car cps)) && ((inTurnPiece g car cps) && (overHalf g (ourPiecePosition car cps)))) =
        ("Leaving a corner, full throttle!", [throttleMessage 1.0])
    | inTurnPiece g car cps =
        ("In corner", [throttleMessage 0.5])
    | distanceToNextCorner g (ourPiecePosition car cps) <= 1 =
        ("Nearing a corner", [throttleMessage 0.1])
    | (isNextATurn g car cps) || distanceToNextCorner g (ourPiecePosition car cps) <= 4 =
        ("Slow for corner entry", [throttleMessage 0.43])
    | distanceToNextCorner g (ourPiecePosition car cps) >= 9 =
        ("Straight away!", [throttleMessage 1.0])
    | otherwise = ("throttle 0.65", [throttleMessage 0.65])

respond :: GameInitData -> CarId -> ServerMessage -> IO ([ClientMessage], CarId, GameInitData)
respond game car message = case message of
  Join -> do
    putStrLn "Joined"
    return ([pingMessage], car, game)
  GameInit gameInit -> do
    return ([pingMessage], car, gameInit)
  CarPositions carPositions -> do
    let result = whatDo game car carPositions
    return $ ((snd result), car, game)
  YourCar newCar -> do
    return ([pingMessage], newCar, game)
  Unknown msgType -> do
    putStrLn $ "Unknown message: " ++ msgType
    return ([pingMessage], car, game)

decodeMessage :: (String, Value) -> Result ServerMessage
decodeMessage (msgType, msgData)
  | msgType == "join" = Success Join
  | msgType == "yourCar" = YourCar <$> (fromJSON msgData)
  | msgType == "gameInit" = GameInit <$> (fromJSON msgData)
  | msgType == "carPositions" = CarPositions <$> (fromJSON msgData)
  | otherwise = Success $ Unknown msgType

instance FromJSON a => FromJSON (String, a) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    return (msgType, msgData)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)
